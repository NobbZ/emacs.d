;;; init-stackexchange --- initialises stack exchange client

;;; Commentary:

;;; Code:

(require 'init-elpa)

(when (maybe-require-package 'sx)
  (defvar launcher-map (make-sparse-keymap))

  ;; TODO: Move this into its own package
  (define-prefix-command 'launcher-map)
  (global-set-key (kbd "<f12>") 'launcher-map)
  (define-key launcher-map "qq" #'sx-tab-all-questions)
  (define-key launcher-map "qi" #'sx-inbox)
  (define-key launcher-map "qo" #'sx-open-link)
  ;; (define-key launcher-map "qu" #'sx-tab-unanswerd-my-tags)
  (define-key launcher-map "qa" #'sx-ask)
  (define-key launcher-map "qs" #'sx-search)

  (which-key-add-key-based-replacements
    "<f12>"     "Applications"
    "<f12> q"   "Stackexchange"
    "<f12> q q" "All questions"))

(provide 'init-stackexchange)
;;; init-stackexchange.el ends here
