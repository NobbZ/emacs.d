;; (require-package 'ample-theme)

;; (load-theme 'ample       t t)
;; (load-theme 'ample-flat  t t)
;; (load-theme 'ample-light t t)
;; (enable-theme 'ample)

(require-package 'color-theme-sanityinc-solarized)
(require-package 'color-theme-sanityinc-tomorrow)

;; Unless you customize, you'll get this theme
(setq-default custom-enabled-themes '(sanityinc-tomorrow-bright))

;; Ensure themes will be applied even if they have not been customized
(defun reapply-themes ()
  "Forcibly load the themes listed in `custom-enabled-themes'."
  (dolist (theme custom-enabled-themes)
    (unless (custom-theme-p theme)
      (load-theme theme t)))
  (custom-set-variables `(custom-enabled-themes (quote ,custom-enabled-themes))))

(add-hook 'after-init-hook 'reapply-themes)

(when (maybe-require-package 'telephone-line)
  (setq-default
   telephone-line-lhs '((accent . (telephone-line-vc-segment
                                   telephone-line-erc-modified-channels-segment
                                   telephone-line-process-segment))
                        (nil    . (telephone-line-minor-mode-segment
                                   telephone-line-buffer-segment)))
   telephone-line-rhs '((nil    . (telephone-line-misc-info-segment))
                        (accent . (telephone-line-major-mode-segment))
                        (accent . (telephone-line-airline-position-segment))))

  (telephone-line-mode t))

(provide 'init-optics)
