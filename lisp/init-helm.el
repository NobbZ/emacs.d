(when (maybe-require-package 'helm)
  (require 'helm)
  (helm-autoresize-mode t)

  (setq-default
   helm-autoresize-max-height 20  ; take 20% of the screen at most
   helm-autoresize-min-height  1) ; get as small as necessary

  (global-set-key (kbd "M-x")     'helm-M-x)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-x C-b") 'helm-buffers-list)

  (helm-mode t))

(provide 'init-helm)
