(require-package 'gitignore-mode)
(require-package 'gitconfig-mode)
(maybe-require-package 'git-timemachine)

(when (maybe-require-package 'magit)
  (setq-default magit-diff-refine-hunk t)

  ;; Hint: customize `magit-repository-directories' so that you can use C-u M-F12
  ;; to quickly open magit on any one of your projects.
  (global-set-key [(meta f12)]    'magit-status)
  (global-set-key (kbd "C-x g")   'magit-status)
  (global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

  ;; TODO: configure `whichkey'
  )

(after-load 'magit
  (define-key magit-status-mode-map (kbd "C-M-<up>") 'magit-section-up)
  (add-hook 'magit-popup-mode-hook 'sanityinc/no-trailing-whitespace))

(when (maybe-require-package 'magit-todos)
  (after-load 'magit
    (magit-todos-mode)))

(provide 'init-git)
