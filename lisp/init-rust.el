;;; init-rust --- Initialises rust mode and its language server

;;; Commentary:

;;; Code:

(require-package 'rust-mode)
(require-package 'eglot)

(provide 'init-rust)
;;; init-rust.el ends here
