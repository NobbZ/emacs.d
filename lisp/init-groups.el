(require-package 'f)
(require-package 's)

(require 'f)
(require 's)

(defconst nobbz/home-dir (getenv "HOME"))

(defconst nobbz/groups-dir (f-join nobbz/home-dir ".config" "nobbz" "hosts"))

(defconst nobbz/groups-file (f-join nobbz/groups-dir (s-concat (system-name) ".groups")))

(defconst nobbz/group-list
  (if (file-exists-p nobbz/groups-file)
      (with-temp-buffer
        (insert-file-contents nobbz/groups-file)
        (split-string (buffer-string) "\n" t))
    (progn
      (message "No groupfile exists!")
      '())))

(defun nobbz/group-member? (group-identifier)
  "Return non-nil if this system is a member of GROUP-IDENTIFIER.

GROUP-IDENTIFIER can be either a symbol, a string or a sequence thereof."
  (if (seqp group-identifier)
      (seq-some #'nobbz/group-member? group-identifier)
    (let ((grp-name (if (symbolp group-identifier)
                        (symbol-name group-identifier)
                      group-identifier)))
      (member grp-name nobbz/group-list))))

(provide 'init-groups)
