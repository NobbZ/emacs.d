(when (maybe-require-package 'erlang)
  (require 'erlang-start))

(when (maybe-require-package 'company-erlang)
  (add-hook 'erlang-mode-hook #'company-erlang-init))

(provide 'init-erlang)
