(when (fboundp 'prettify-symbols-mode)
  (add-hook 'emacs-lisp-mode-hook #'prettify-symbols-mode)
  (add-hook 'elisp-mode-hook #'prettify-symbols-mode)
  (add-hook 'lisp-mode-hook #'prettify-symbols-mode))

(when (maybe-require-package 'flycheck-cask)
  (add-hook 'flycheck-mode-hook #'flycheck-cask-setup))

(when (maybe-require-package 'flycheck-package)
  (add-hook 'after-init-hook #'flycheck-package-setup))

(when (maybe-require-package 'smartparens)
  (require 'smartparens-config)
  (add-hook 'emacs-lisp-mode-hook #'smartparens-mode)
  (add-hook 'elisp-mode-hook #'smartparens-mode)
  (add-hook 'lisp-mode-hook #'smartparens-mode))

(when (maybe-require-package 'rainbow-delimiters)
  (add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'elisp-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'lisp-mode-hook #'rainbow-delimiters-mode))

(setq-default show-paren-style 'expression)

(add-hook 'emacs-lisp-mode-hook (lambda () (show-paren-mode t)))
(add-hook 'elisp-mode-hook (lambda () (show-paren-mode t)))
(add-hook 'lisp-mode-hook (lambda () (show-paren-mode t)))

(provide 'init-lisp)
