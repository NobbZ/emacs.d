;;; init-idris --- Initialises necessary things for Idris language tools

;;; Commentary:

;;; Code:

(require 'init-elpa)

(require-package 'idris-mode)

(provide 'init-idris)
;;; init-idris.el ends here
