;; enable displaying whitespace everywhere
(global-whitespace-mode)

;; Change some colors of whitespace
(set-face-attribute 'whitespace-space nil :background "black" :foreground "gray30")
(set-face-attribute 'whitespace-tab   nil :background "black" :foreground "gray30")

;; Clean whitespace on save
(add-to-list 'before-save-hook #'whitespace-cleanup)

;; set max line width to something that will not be reached,
;; so that `whitespace-mode' won't color those lines.
(setq-default whitespace-line-column 999999)

;; spaces instead of tab-stops
(setq-default indent-tabs-mode nil)

;; require the final newline
(setq-default require-final-newline 'visit-save)

(require-package 'beacon)
(beacon-mode t)

(global-linum-mode t)

(defun sanityinc/no-trailing-whitespace ()
  "Turn off display of trailing whitespace in this buffer."
  (setq show-trailing-whitespace nil))

(provide 'init-whitespace)
