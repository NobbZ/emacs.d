(when (maybe-require-package 'projectile)
  (projectile-mode t)

  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

  (require-package 'helm-projectile))

(setq projectile-project-search-path '("~/projects/" "~/go"))

(provide 'init-projectile)
