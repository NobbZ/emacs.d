(require-package 'go-mode)
(require-package 'company-go)
(require-package 'flycheck-gometalinter)
(require-package 'go-playground)

(add-hook 'before-save-hook 'gofmt-before-save)
(add-hook 'go-mode-hook
          (lambda ()
            (setq tab-width 4)))

;; Make emacs pick up the proper path
(add-to-list 'exec-path
             (format "%s/bin" (string-trim (shell-command-to-string "go env GOPATH"))))

(defun nobbz/install-go-tool (name repository)
  (let ((path (executable-find name)))
    (if path
        (message "Go tool `%s' installed at `%s'" name path)
      (message "Installing go tool `%s' from `%s'" name repository)
      (call-process-shell-command (format "go get %s" repository))
      (message "Finished installing got tool `%s' from `%s' to `%s'" name repository (executable-find name)))))

(defvar nobbz/go-tools-alist
  '(("godef" . "github.com/rogpeppe/godef")))

(dolist (go-tool nobbz/go-tools-alist)
  (let ((name (car go-tool))
        (repo (cdr go-tool)))
    (nobbz/install-go-tool name repo)))

(provide 'init-golang)
