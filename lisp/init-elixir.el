;;; init-elixir --- Initialises necessary things for elixir LS and alchemist

;;; Commentary:

;;; Code:

(require 'init-elpa)
(require 'init-lsp)

(require-package 'elixir-mode)

(require-package 'eglot)
(require 'eglot)
(add-to-list
 'eglot-server-programs
 '(elixir-mode . ("sh" "/home/nmelzer/.elixir-ls/release/language_server.sh")))

(add-hook 'elixir-mode-hook
          (lambda ()
            (subword-mode)
            (eglot-ensure)
            (company-mode)
            (flymake-mode)
            (add-hook 'before-save-hook 'eglot-format nil t)))



;; (if nobbz/lsp-mode-available
;;     (progn
;;       (require-package 'elixir-mode)

;;       (dolist (p '(alchemist-elixir-ls alchemist-goto alchemist))
;;         (require p))))

(provide 'init-elixir)
;;; init-elixir.el ends here
