(require-package 'yaml-mode)

(dolist (ending '("\\.yml\\'" "\\.yaml\\'"))
  (add-to-list 'auto-mode-alist `(,ending . yaml-mode)))

(provide 'init-yaml)
