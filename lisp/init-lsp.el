(defconst nobbz/lsp-mode-available (version<= "25.1" emacs-version)
  "Tell whether or not the LSP mode is available.")

(if nobbz/lsp-mode-available
    (dolist (p '(lsp-mode lsp-ui company-lsp))
      (require-package p)))

(provide 'init-lsp)
