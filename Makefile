EL_FILES  = $(sort $(shell find ./lisp -type f -name '*.el') init.el ${GEN_EL_FILES})
ELC_FILES = ${EL_FILES:%.el=%.elc}

EDITOR_CRUFT_FILES = $(shell find . -type f -name '*~')

.INTERMEDIATE: ${GEN_EL_FILES}

all: byte-compile

lint: ${EL_FILES:%=%-lint}

byte-compile: ${ELC_FILES}

update:
	echo "not implemented yet"
.PHONY: update

clean: ${ELC_FILES:%=%-rm-f}

rebuild: clean all

full-clean: clean ${EDITOR_CRUFT_FILES:%=%-rm-f}

# CLEAN Helpers

%-rm-f:
	rm -f $*

%-rm-rf:
	rm -rf $*

# Lint helpers

%.el-lint:
	emacs -Q --batch -l elisp-lint.el -f elisp-lint-files-batch --no-indent-character $*.el

# Build helpers

%.elc: %.el
	emacs -nw --batch \
		--eval '(byte-compile-file "$<")'

elisp-lint.el:
	curl https://raw.githubusercontent.com/gonewest818/elisp-lint/master/elisp-lint.el > elisp-lint.el
.PHONY: elisp-lint.el
