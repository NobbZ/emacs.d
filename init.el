;;; init --- Initialises my configuration

;;; Commentary:

;; this file bootstraps the configuration, which is divided into
;; a number of other files.

;;; Code:

;; (package-initialize)

;; Allow flycheck to find required packages
(setq-default flycheck-emacs-lisp-initialize-packages 'auto)
(add-to-list 'safe-local-variable-values
             '(flycheck-emacs-lisp-load-path "./lisp" "."))

(message "extending loadpath")
(add-to-list 'load-path
             (expand-file-name "lisp" user-emacs-directory))
(require 'init-benchmarking)

;; ------------------------------------------------------------------
;; Bootstrap config
;; ------------------------------------------------------------------
(message "Bootstrapping")
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(require 'init-utils)
(require 'init-site-lisp)
(require 'init-elpa)
(require 'init-exec-path)

(require-package 'dash)

;; ------------------------------------------------------------------
;; Initialise the groups system
;; ------------------------------------------------------------------
;; This system is used to identify my machines and load a different
;; set of packages depending on the group membership.

(require 'init-groups)

;; ------------------------------------------------------------------
;; Load config for specific features
;; ------------------------------------------------------------------

(require-package 'wgrep)
(require-package 'diminish)
(require-package 'scratch)
(require-package 'command-log-mode)
(require-package 'tablist)

(require 'init-optics)

(require 'init-whichkey)
(require 'init-whitespace)
(require 'init-helm)
(require 'init-company)
(require 'init-flycheck)
(require 'init-projectile)

;; -------------------------------------------------------------------
;; Load configuration for version control systems (and their aux)
;; -------------------------------------------------------------------

(require 'init-git)

;; -------------------------------------------------------------------
;; Load configuration for applications
;; -------------------------------------------------------------------

(require 'init-stackexchange)

;; -------------------------------------------------------------------
;; Load configuration for config language modes
;; -------------------------------------------------------------------

(require 'init-yaml)

;; -------------------------------------------------------------------
;; Load configuration for programming language modes
;; -------------------------------------------------------------------

(if (nobbz/group-member? '(erlang elixir))
    (require 'init-erlang))

(if (nobbz/group-member? 'elixir)
    (require 'init-elixir))

(if (nobbz/group-member? 'golang)
    (require 'init-golang))

(if (nobbz/group-member? '(lisp emacs))
    (require 'init-lisp))

(if (nobbz/group-member? 'idris)
    (require 'init-idris))

(if (nobbz/group-member? '(rust))
    (require 'init-rust))

;; -------------------------------------------------------------------
;; allow access from emacs-client
;; -------------------------------------------------------------------

(require 'server)
(unless (server-running-p)
  (server-start))

(provide 'init)
;;; init.el ends here
