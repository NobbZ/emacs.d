;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (flycheck-emacs-lisp-load-path "./lisp" "."))
 (emacs-lisp-mode
  (fill-column . 120)))
