#+TITLE: README first
#+AUTHOR: Norbert Melzer
#+EMAIL: timmelzer+emacs@gmail.com
#+LANGUAGE: en

* An Emacs config tailored for me

This is a work in progress Emacs configuration, which I strcture
similar to that of [[https://github.com/purcell/emacs.d/][Steve Purcell]]. I will swap out a lot of the
languages though and specific settings I'm not comfortable with.

Still, I have taken some of the code he uses as well.

There is no guarantee that anything of this will actually work on
systems not owned or maintained by me.

* Installation

Just clone this repository into your =~/.emacs.d=, after making sure
you have backed up your old configuration:

#+BEGIN_SRC sh
  [ -d ~/.emacs.d ] && mv ~/.emacs.d.$(date -I)
  git clone https://gitlab.com/NobbZ/emacs.d.git ~/.emacs.d/
#+END_SRC

This will create a backup if there was already an emacs configuration
available, suffixed by the current date and then clone the repository.

When you start ~emacs~ for the first time after cloning the
repository, starting it may take a long time, as packages are
downloaded from the internet as necessary.

* TODO Languages [0/3]
** TODO Programming languages [1/2]
*** TODO Erlang
*** DONE Elixir

Installing elixir was pretty straight forward.

+ It was necessary to clone a fork of alchemist.el into the ~load-path~. This fork is a special fork which is LSP enabled
+ It was necessary to install the following packages:
  + ~company-lsp~
  + ~company-quickhelp~
  + ~company~
  + ~elixir-mode~
  + ~lsp-ui~
  + ~lsp-mode~
+ Load the following packages:
  + ~alchemist-elixir-ls~
  + ~alchemist-goto~
  + ~alchemist~

** TODO Markup languages [0/2]
*** TODO Markdown
*** TODO Org
** TODO Serialisation/Config languages [1/2]
*** TODO TOML
*** DONE Yaml
