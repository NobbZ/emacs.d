#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o xtrace

if [ -n ${CI} ]; then
    # Make it look like this were ~/.emacs.d (needed by some older versions of emacs)
    export HOME=$(pwd)/..
    ls -lah
    ls -lah ..
    mkdir -p ../.emacs.d
    ln -s emacs.d ../.emacs.d/
fi

echo "Attempting startup"

${EMACS:=emacs} -nw --batch \
                --eval '(let ((debug-on-error t)
                              (url-show-status nil)
                              (user-emacs-directory default-directory)
                              (user-init-file (expand-file-name "init.el"))
                              (load-path (delq default-directory load-path)))
                           (load-file user-init-file)
                           (run-hooks (quote after-init-hook)))'

echo "Startup successfull"
